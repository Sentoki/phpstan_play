<?php

namespace App;

class SomeClass
{
    public function helloWorld()
    {
        echo "\nhello world\n";
    }

    public function func1(): string
    {
        return null;
    }
}
